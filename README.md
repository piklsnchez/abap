<h1>owner allocation rest webservices</h1>

<h1>ycl_owner_allocation_rest class contains the url templates for the services:</h1>
<p>for dev environment sap-client parameter is required, use 300; saml2=disabled is required to prevent sso, we use basic auth</p>

<h1>ycl_owner_allocation_provider class contains the create, read, update, and delete function for the owner allocation and their properties:</h1>
<p>examble request curl 'https://s4hdev.swgas.com/zrestowneralloc/owner_allocation/1200000166?sap-client=300&saml2=disabled'  -H 'Authorization: Basic ...'</p>
<p>/owner_allocation/{owner}/{?property} URI has GET, POST, and DELETE methods;<br>
    for POST and DELETE the caller must obtain a CSRF token by making a GET request (any GET request) with the header "X-CSRF-TOKEN: Fetch" and set the header for the subsequent POST/DELETE/etc. request along with the cookies sent back from the GET request.<br>
  input and output json:<br>
    GET output {"owner":"<owner>", "properties":[{"property":"<property>","contract_account":"<contract_account>","premise":"<premise>"},...]}<br>
    POST input {"contract_account": "<contract_account>", "premise": "<premise>"}<br>
         output {"property": "<property>", "contract_account":"<contract_account>"}<br>
    DELETE input - none use the property path parameter<br>
</p>
<pre>
#!/bin/bash
BP="1200000166"
URL="https://s4hdev.swgas.com/zrestowneralloc/owner_allocation/${BP}?sap-client=300&saml2=disabled"
AUTH=""

function get_accounts(){
  curl --http1.1 --silent "${URL}" -H "Authorization: Basic ${AUTH}" -H "X-CSRF-TOKEN: Fetch" -b "cookie.txt" -c "cookie.txt" -D header.txt -o output.txt
  cat output.txt
  echo
}

function get_token(){
  tokenLine=$(grep ^x-csrf-token: header.txt)
  read -a _token <<< "${tokenLine}"
  token=${_token[1]}
  echo ${token}
}

function create_account(){
  contract_account=
  premise=5200017209
  POST="{\"contract_account\":\"${contract_account}\",\"premise\":\"${premise}\"}"
  curl --http1.1 --silent "${URL}" -H "Authorization: Basic ${AUTH}" -H "x-csrf-token: ${1}" --data "${POST}" -b "cookie.txt" -c "cookie.txt" -D header.txt
  echo
}

function delete_account(){
  property=$(jq -r .PROPERTIES[0].PROPERTY < output.txt)
  URL="https://s4hdev.swgas.com/zrestowneralloc/owner_allocation/${BP}/${property}?sap-client=300&saml2=disabled"
  curl --http1.1 --silent --request "DELETE" "${URL}" -H "Authorization: Basic ${AUTH}" -H "x-csrf-token: ${1}" -b "cookie.txt" -c "cookie.txt" -D header.txt
  echo
}

get_accounts
cat header.txt
token=$(get_token)
echo $token
create_account $token
cat header.txt
delete_account $token
cat header.txt
</pre>
<h1>ycl_owner_allocation_mailing class contains the crud methods for mailing addresses associated with the owner contract account:</h1>
<h2>UPDATED single address no longer an array</h2>
<p>/mailing/{owner}/{contract_account} URI has GET and PUT methods;<br>
    same as previous non-GET calls must obtain a csrf token first.<br>
  owner and contract_account parameters in the url as https://s4hdev.swgas.com/zrestowneralloc/mailing/1200000166/910000000168<br>
  input and output json:<br>
    GET output {"mailing_address":{id,number,street,apt,city,state,post_code,po_box,country}}<br>
<pre>
#!/bin/bash
owner="1200000166"
contract_account="910000019100"
URL="https://s4hdev.swgas.com/zrestowneralloc/mailing/${owner}/${contract_account}?sap-client=300&saml2=disabled"
AUTH="<your auth here>"

function get_token(){
  tokenLine=$(grep ^x-csrf-token: header.txt)
  read -a _token <<< "${tokenLine}"
  token=${_token[1]}
  echo ${token}
}

function get_mailing_address(){
  curl -k --http1.1 --silent "${URL}" -H "Authorization: Basic ${AUTH}" -H "X-CSRF-TOKEN: Fetch" -b cookie.txt -c cookie.txt -D header.txt -o output.txt
  cat output.txt
  echo
}

function create_mailing_address(){
  curl -k --http1.1 --silent "${URL}" -H "Authorization: Basic ${AUTH}" -H "x-csrf-token: ${1}" --data '{"id":"","number":"123","street":"fake st","apt":"","city":"las vegas","state":"NV","post_code":"89179","po_box":"","country":"US"}' -X "PUT" -b cookie.txt -c cookie.txt -D header.txt -o output.txt
  cat output.txt
  echo
}

function update_mailing_address(){
  echo
}

get_mailing_address
cat header.txt

token=$(get_token)
echo $token
create_mailing_address $token
cat header.txt
</pre>
</p>
<h1>ycl_owner_allocation_contact class contains the crud methods for contact info, i.e. phone, mobile, email:</h1>
<p>/contact_info/all/{owner}/{contract_account} URI has GET method to get all contact info</p>
<p>GET output {"OWNER":"<owner>","INFO":[{"ID":"<id>","PHONE_NUMBER":"<phone_number>","MOBILE_NUMBER":"<mobile_number>","EMAIL_ADDRESS":"<email_address>"}]</p>
<p>/contact_info/phone/{owner}/{contract_account}</p>
<p>/contact_info/mobile/{owner}/{contract_Account}</p>
<p>/contact_info/email/{owner}/{contract_account}</p>
<pre>
#!/bin/bash
AUTH=""
bp=1200000166
account=910000019100

function get_token(){
  tokenLine=$(grep ^x-csrf-token: header.txt)
  read -a _token <<< "${tokenLine}"
  token=${_token[1]}
  echo ${token}
}

function get_all(){
  curl -k --http1.1 --silent "https://s4hdev.swgas.com/zrestowneralloc/contact_info/all/${bp}/${account}?sap-client=300&saml2=disabled" -H "authorization: Basic ${AUTH}" -H "X-CSRF-TOKEN: Fetch" -b cookie.txt -c cookie.txt -D header.txt -o output.txt
  cat output.txt
  echo
}

function get_phone(){
  curl -k --http1.1 --silent "https://s4hdev.swgas.com/zrestowneralloc/contact_info/phone/${bp}/${account}?sap-client=300&saml2=disabled" -H "authorization: Basic ${AUTH}" -H "X-CSRF-TOKEN: Fetch" -b cookie.txt -c cookie.txt -D header.txt -o output.txt
  cat output.txt
  echo
}

function get_mobile(){
  curl -k --http1.1 --silent "https://s4hdev.swgas.com/zrestowneralloc/contact_info/mobile/${bp}/${account}?sap-client=300&saml2=disabled" -H "authorization: Basic ${AUTH}" -H "X-CSRF-TOKEN: Fetch" -b cookie.txt -c cookie.txt -D header.txt -o output.txt
  cat output.txt
  echo
}

function get_email(){
  curl -k --http1.1 --silent "https://s4hdev.swgas.com/zrestowneralloc/contact_info/email/${bp}/${account}?sap-client=300&saml2=disabled" -H "authorization: Basic ${AUTH}" -H "X-CSRF-TOKEN: Fetch" -b cookie.txt -c cookie.txt -D header.txt -o output.txt
  cat output.txt
  echo
}

function set_all(){
  data="{\"id\":\"\",\"phone_number\":\"5551212\",\"mobile_number\":\"5552345\",\"email_address\":\"chris@hotmail.com\"}"
  curl -k --http1.1 --silent "https://s4hdev.swgas.com/zrestowneralloc/contact_info/all/${bp}/${account}?sap-client=300&saml2=disabled" -H "authorization: Basic ${AUTH}" -H "x-csrf-token: ${1}" -X "PUT" --data ${data} -b cookie.txt -c cookie.txt -D header.txt -o output.txt
  cat header.txt
  cat output.txt
  echo
}

function set_phone(){
  data="{\"id\":\"\",\"phone_number\":\"5551213\"}"
  curl -k --http1.1 --silent "https://s4hdev.swgas.com/zrestowneralloc/contact_info/phone/${bp}/${account}?sap-client=300&saml2=disabled" -H "authorization: Basic ${AUTH}" -H "x-csrf-token: ${1}" -X "PUT" --data ${data} -b cookie.txt -c cookie.txt -D header.txt -o output.txt
  cat header.txt
  cat output.txt
  echo
}

function set_mobile(){
  data="{\"id\":\"\",\"mobile_number\":\"5552346\"}"
  curl -k --http1.1 --silent "https://s4hdev.swgas.com/zrestowneralloc/contact_info/mobile/${bp}/${account}?sap-client=300&saml2=disabled" -H "authorization: Basic ${AUTH}" -H "x-csrf-token: ${1}" -X "PUT" --data ${data} -b cookie.txt -c cookie.txt -D header.txt -o output.txt
  cat header.txt
  cat output.txt
  echo
}

function set_email(){
  data="{\"id\":\"\",\"email_address\":\"chris@dubose.xyz\"}"
  curl -k --http1.1 --silent "https://s4hdev.swgas.com/zrestowneralloc/contact_info/email/${bp}/${account}?sap-client=300&saml2=disabled" -H "authorization: Basic ${AUTH}" -H "x-csrf-token: ${1}" -X "PUT" --data ${data} -b cookie.txt -c cookie.txt -D header.txt -o output.txt
  cat header.txt
  cat output.txt
  echo
}

echo "get_all"
get_all
token=$(get_token)
echo "token: ${token}"
echo "set_all"
set_all $token
echo "get_phone"
get_phone
echo "set_phone"
set_phone $token
get_phone
echo "get_mobile"
get_mobile
echo "set_mobile"
set_mobile $token
get_mobile
echo "get_email"
get_email
echo "set_email"
set_email $token
get_email
</pre>
<h1>ycl_owner_allocation_service class contains the crud methods for getting and creating service orders</h1>
<p>/service_order/{owner} URI has GET method (thus far) to get all the service orders associated to the BP<br>
  WIP<br>
</p>
<h1>ycl_owner_allocation_premise get method for getting information about premise, i.e. owner, contract account</h1>
<p>/premise_info/{premise} URI has GET method</p><br>
WIP<br>
<h1>ycl_owner_allocation_manager class contains crud methods for adding and removing property manager</h1>
<p>/manager/{owner}/{property} URI has GET, PUT, POST, and DELETE methods</p>
<pre>
#!/bin/bash
BP="1200000167"
MANAGER="1200006922"
PROPERTY="0000000009"
URL="https://s4hdev.swgas.com/zrestowneralloc/manager/${BP}/${PROPERTY}?sap-client=300&saml2=disabled"
AUTH="<auth>"

function get_token(){
  tokenLine=$(grep ^x-csrf-token: header.txt)
  read -a _token <<< "${tokenLine}"
  token=${_token[1]}
  echo ${token}
}

function get_manager(){
  curl --http1.1 --silent "${URL}" -H "authorization: Basic ${AUTH}" -H "X-CSRF-TOKEN: Fetch" -b "cookie.txt" -c "cookie.txt" -D header.txt -o output.txt
  echo
}

function add_manager(){
  post_data="{\"manager\": \"${MANAGER}\"}"
  curl --http1.1 --silent -XPUT "${URL}" -H "authorization: Basic ${AUTH}" -H "x-csrf-token: ${1}" --data "${post_data}" -b "cookie.txt" -c "cookie.txt" -D header.txt -o output.txt
  echo
}

function remove_manager(){
  post_data="{\"manager\": \"${MANAGER}\"}"
  curl --http1.1 --silent -XDELETE "${URL}" -H "authorization: Basic ${AUTH}" -H "x-csrf-token: ${1}" --data "${post_data}" -b cookie.txt -c cookie.txt -D header.txt -o output.txt
  echo
}

get_manager
cat output.txt
token=$(get_token)
add_manager ${token}
cat header.txt
get_manager
cat output.txt
echo
remove_manager ${token}
cat header.txt
echo
get_manager
cat output.txt
echo
</pre>
<h1>ycl_owner_allocation_billing class contains get methods for billing info</h1>
<p>/billing_info/{owner}/{?contract_account} URI has GET methods for bp or bp and contract account; to get for one contract account or all associated with the bp</p>
<pre>
#!/bin/bash
BP="1200000166"
CA="910000000168"
BP_URL="https://s4hdev.swgas.com/zrestowneralloc/billing_info/${BP}?sap-client=300&saml2=disabled"
CA_URL="https://s4hdev.swgas.com/zrestowneralloc/billing_info/${BP}/${CA}?sap-client=300&saml2=disabled"
AUTH="Y3NkMzpTIWxlbmNlb2Z0aGVsYW1iczE="

function get_token(){
  tokenLine=$(grep ^x-csrf-token: header.txt)
  read -a _token <<< "${tokenLine}"
  token=${_token[1]}
  echo ${token}
}

function get_billing_info(){
  curl --http1.1 --silent "${CA_URL}" -H "authorization: Basic ${AUTH}" -H "X-CSRF-TOKEN: Fetch" -b "cookie.txt" -c "cookie.txt" -D "header.txt" -o "output.txt"
  cat output.txt
}

function get_billings_info(){
  curl --http1.1 --silent "${BP_URL}" -H "authorization: Basic ${AUTH}" -H "X-CSRF-TOKEN: Fetch" -b "cookie.txt" -c "cookie.txt" -D "header.txt" -o "output.txt"
  cat output.txt
}

function post_payment(){
  curl -k --http1.1 --silent "${CA_URL}" -H "authorization: Basic ${AUTH}" -H "x-csrf-token: ${1}" --data '{"amount":1.00}' -X PUT -b "cookie.txt" -c "cookie.txt" -D "header.txt" -o "output.txt"
  cat output.txt
  echo
}

get_billing_info
echo
#get_billings_info
#echo

token=$(get_token)
echo $token
post_payment $token
echo
</pre>
