CLASS ycl_owner_allocation_contact DEFINITION
  PUBLIC
  INHERITING FROM cl_rest_resource
  FINAL
  CREATE PUBLIC .

PUBLIC SECTION.
  METHODS:
    constructor
  , if_rest_resource~get REDEFINITION
  , if_rest_resource~put REDEFINITION
  , get_all
    IMPORTING
      owner TYPE string
      account TYPE string
    RETURNING VALUE(result) TYPE ycl_owner_allocation_dao=>contact_info
  , set_all
    IMPORTING
      owner TYPE string
      account TYPE string
      contact_info TYPE ycl_owner_allocation_dao=>contact_info_type
    RETURNING VALUE(result) TYPE ycl_owner_allocation_dao=>contact_info_type
    RAISING zcx_data_access_exception
  , get_phone
    IMPORTING
      owner TYPE string
      account TYPE string
    RETURNING VALUE(result) TYPE string
  , set_phone
    IMPORTING
      owner TYPE string
      account TYPE string
      contact_info TYPE ycl_owner_allocation_dao=>contact_info_type
    RETURNING VALUE(result) TYPE ycl_owner_allocation_dao=>contact_info_type
    RAISING zcx_data_access_exception
  , get_mobile
    IMPORTING
      owner TYPE string
      account TYPE string
    RETURNING VALUE(result) TYPE string
  , set_mobile
    IMPORTING
      owner TYPE string
      account TYPE string
      contact_info TYPE ycl_owner_allocation_dao=>contact_info_type
    RETURNING VALUE(result) TYPE ycl_owner_allocation_dao=>contact_info_type
    RAISING zcx_data_access_exception
  , get_email
    IMPORTING
      owner TYPE string
      account TYPE string
    RETURNING VALUE(result) TYPE string
  , set_email
    IMPORTING
      owner TYPE string
      account TYPE string
      contact_info TYPE ycl_owner_allocation_dao=>contact_info_type
    RETURNING VALUE(result) TYPE ycl_owner_allocation_dao=>contact_info_type
    RAISING zcx_data_access_exception
  .
  CLASS-METHODS:
    class_constructor
  .
PROTECTED SECTION.
PRIVATE SECTION.
  CLASS-DATA:
    logger TYPE REF TO zcl_log_util
  .
  DATA:
    dao TYPE REF TO ycl_owner_allocation_dao
  .
ENDCLASS.

CLASS ycl_owner_allocation_contact IMPLEMENTATION.
  METHOD class_constructor.
    logger = NEW zcl_log_util( 'YCL_OWNER_ALLOCATION_CONTACT' ).
  ENDMETHOD.

  METHOD constructor.
    super->constructor( ).
    me->dao = NEW ycl_owner_allocation_dao( ).
  ENDMETHOD.

  METHOD if_rest_resource~get.
  " ! get contact info for owner; phone, mobile, email
    logger->do_info( |{ mo_request->gc_header_http_method } { zcl_object_util=>table_to_string( mo_request->get_uri_attributes( ) ) }| ).
    "path parameters= /<"all"|"phone"|"mobile"|"email">/<owner>
    "request query string=
    "request body= none
    "response= {"owner":"<owner>","info":[{"id":"<id>","phone_number":"<phone_number>","mobile_number":"<mobile_number>","email_address":"<email_address>"}]}
    TRY.
      DATA(owner)   = mo_request->get_uri_attribute( 'owner' ).
      DATA(account) = mo_request->get_uri_attribute( 'contract_account' ).
      dao->set_owner( CONV #( owner ) ).
      DATA(json) = SWITCH #( to_lower( mo_request->get_uri_attribute( 'scope' ) )
        WHEN 'all' THEN
          "get_all
          /ui2/cl_json=>serialize( data = get_all( owner = owner account = account ) )
        WHEN 'phone' THEN
          "get_phone
          /ui2/cl_json=>serialize( data = get_phone( owner = owner account = account ) )
        WHEN 'mobile' THEN
          "get_mobile
          /ui2/cl_json=>serialize( data = get_mobile( owner = owner account = account ) )
        WHEN 'email' THEN
          "get_email
          /ui2/cl_json=>serialize( data = get_email( owner = owner account = account ) )
        "else
          "404
      ).
      mo_response->set_status( cl_rest_status_code=>gc_success_ok ).
      mo_response->create_entity( )->set_string_data( json ).
    CATCH zcx_data_access_exception INTO DATA(dao_exception).
      "TODO: other errors
      mo_response->set_status( cl_rest_status_code=>gc_server_error_internal ).
      mo_response->create_entity( )->set_string_data( |\{error:"{ dao_exception->get_longtext( ) }"\}| ).
    CATCH cx_root INTO DATA(root_exception).
      mo_response->set_status( cl_rest_status_code=>gc_server_error_internal ).
      mo_response->create_entity( )->set_string_data( |\{error:"{ root_exception->get_text( ) }"\}| ).
    ENDTRY.
  ENDMETHOD.

  METHOD if_rest_resource~put.
  " ! set contact info for owner; phone, mobile, email
    logger->do_info( |{ mo_request->gc_header_http_method } { zcl_object_util=>table_to_string( mo_request->get_uri_attributes( ) ) }| ).
    "path parameters= /<"all"|"phone"|"mobile"|"email">/<owner>/<contract_account>
    "request query string=
    "request body= {"id":"","phone_number":"<phone_number>","mobile_number":"<mobile_number>","email_address":"<email_address>"}
    "response= {"id":"<id>","phone_number":"<phone_number>","mobile_number":"<mobile_number>","email_address":"<email_address>"}
    TRY.
      DATA(owner)   = mo_request->get_uri_attribute( 'owner' ).
      DATA(account) = mo_request->get_uri_attribute( 'contract_account' ).
      dao->set_owner( CONV #( owner ) ).
      DATA contact_info TYPE ycl_owner_allocation_dao=>contact_info_type.
      /ui2/cl_json=>deserialize( EXPORTING json = mo_request->get_entity( )->get_string_data( ) CHANGING data = contact_info ).
      DATA(json) = SWITCH #( to_lower( mo_request->get_uri_attribute( 'scope' ) )
        WHEN 'all' THEN
          "set_all
          /ui2/cl_json=>serialize( data = set_all( owner = owner account = account contact_info = contact_info ) )
        WHEN 'phone' THEN
          "set_phone
          /ui2/cl_json=>serialize( data = set_phone( owner = owner account = account contact_info = contact_info ) )
        WHEN 'mobile' THEN
          "set_mobile
          /ui2/cl_json=>serialize( data = set_mobile( owner = owner account = account contact_info = contact_info ) )
        WHEN 'email' THEN
          "set_email
          /ui2/cl_json=>serialize( data = set_email( owner = owner account = account contact_info = contact_info ) )
        "else
          "404
      ).
      dao->commit( ).
      mo_response->set_status( cl_rest_status_code=>gc_success_created ).
      mo_response->create_entity( )->set_string_data( json ).
    CATCH zcx_data_access_exception INTO DATA(dao_exception).
      dao->rollback( ).
      mo_response->set_status( cl_rest_status_code=>gc_server_error_internal ).
      mo_response->create_entity( )->set_string_data( |\{error:"{ dao_exception->get_longtext( ) }"\}| ).
    CATCH cx_root INTO DATA(root_exception).
      dao->rollback( ).
      mo_response->set_status( cl_rest_status_code=>gc_server_error_internal ).

      DATA error_message TYPE string.
      DATA(exception) = root_exception.
      error_message = exception->get_text( ).
      WHILE exception->previous IS NOT INITIAL.
        exception = exception->previous.
        error_message = |{ error_message }; { exception->get_text( ) }|.
      ENDWHILE.
      mo_response->create_entity( )->set_string_data( |\{error:"{ error_message }"\}| ).
    ENDTRY.
  ENDMETHOD.

  METHOD get_all.
    result = dao->get_all_contact_info( contract_account = conv #( account ) ).
  ENDMETHOD.

  METHOD set_all.
    result = contact_info.
    result-id = dao->set_all_contact_info( contract_account = conv #( account ) contact_info = contact_info ).
  ENDMETHOD.

  METHOD get_phone.
    result = dao->get_phone_number( contract_account = conv #( account ) ).
  ENDMETHOD.

  METHOD set_phone.
    result = contact_info.
    result-id = dao->set_phone_number( contract_account = conv #( account ) phone_number = contact_info-phone_number ).
  ENDMETHOD.

  METHOD get_mobile.
    result = dao->get_mobile_number( contract_account = conv #( account ) ).
  ENDMETHOD.

  METHOD set_mobile.
    result = contact_info.
    result-id = dao->set_mobile_number( contract_account = conv #( account ) mobile_number = contact_info-mobile_number ).
  ENDMETHOD.

  METHOD get_email.
    result = dao->get_email_address( contract_account = conv #( account ) ).
  ENDMETHOD.

  METHOD set_email.
    result = contact_info.
    result-id = dao->set_email_address( contract_account = conv #( account ) email_address = contact_info-email_address ).
  ENDMETHOD.

ENDCLASS.